import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input() showBack: boolean;
  @Input() passo: number;
  @Output() passoAnterior: EventEmitter<number> = new EventEmitter();

  lista = [
    {
      passoAnterior: 0
    },
    {
      passoAnterior: 1
    }
  ];

  constructor(private _location: Location) {
  }

  ngOnInit() {
  }

  backClicked() {
    this._location.back();
  }

  voltar() {
    if (this.passo === 0) {
      this._location.back();
    } else {
      this.passoAnterior.emit(this.lista[this.passo - 1].passoAnterior);
    }
  }

}
