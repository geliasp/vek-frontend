import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TaxaComponent} from './taxa.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [TaxaComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [TaxaComponent]
})
export class TaxaModule { }
