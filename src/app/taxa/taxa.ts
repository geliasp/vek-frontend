interface Taxa {
  concorrente: Concorrente;
  debito: {
    taxaConcorrente: number,
    descontoOferecido: number
  };
  credito: {
    taxaConcorrente: number,
    descontoOferecido: number
  };
}
