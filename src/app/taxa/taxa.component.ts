import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-taxa',
  templateUrl: './taxa.component.html',
  styleUrls: ['./taxa.component.css']
})
export class TaxaComponent implements OnInit, OnDestroy {
  formTaxa: FormGroup;

  @Input() taxa: Taxa;
  @Input() concorrentes: Concorrente[];

  @Output() valido: EventEmitter<boolean> = new EventEmitter();

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.formTaxa = this.formBuilder.group({
      taxaDebitoTaxaConcorrente: new FormControl(
        this.taxa.debito.taxaConcorrente,
        [Validators.required, Validators.min(1), Validators.max(100)]
      ),
      taxaDebitoDescontoOferecido: new FormControl(
        this.taxa.debito.descontoOferecido,
        [Validators.required, Validators.min(1), Validators.max(100)]
      ),
      taxaCreditoTaxaConcorrente: new FormControl(
        this.taxa.credito.taxaConcorrente,
        [Validators.required, Validators.min(1), Validators.max(100)]
      ),
      taxaCreditoDescontoOferecido: new FormControl(
        this.taxa.credito.descontoOferecido,
        [Validators.required, Validators.min(1), Validators.max(100)]
      ),
      concorrente: new FormControl(this.taxa.concorrente.id, [Validators.required])
    });
    this.atualizar(false);
  }

  ngOnDestroy(): void {
    this.atualizar(true);
  }

  modelChange() {
    this.atualizar(false);
  }

  atualizar(proximo: boolean) {
    this.taxa.debito.taxaConcorrente = this.taxaDebitoTaxaConcorrente.value;
    this.taxa.debito.descontoOferecido = this.taxaDebitoDescontoOferecido.value;

    this.taxa.credito.taxaConcorrente = this.taxaCreditoTaxaConcorrente.value;
    this.taxa.credito.descontoOferecido = this.taxaCreditoDescontoOferecido.value;

    if (proximo) {
      this.taxa.concorrente.id = this.concorrente.value;
    } else {
      this.valido.emit(this.formTaxa.status === 'VALID');
    }
  }

  get taxaDebitoTaxaConcorrente() {
    return this.formTaxa.get('taxaDebitoTaxaConcorrente');
  }

  get taxaDebitoDescontoOferecido() {
    return this.formTaxa.get('taxaDebitoDescontoOferecido');
  }

  get taxaCreditoTaxaConcorrente() {
    return this.formTaxa.get('taxaCreditoTaxaConcorrente');
  }

  get taxaCreditoDescontoOferecido() {
    return this.formTaxa.get('taxaCreditoDescontoOferecido');
  }

  get concorrente() {
    return this.formTaxa.get('concorrente');
  }

}
