import {Component, OnInit} from '@angular/core';
import {PropostaService} from '../services/proposta.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private propostaService: PropostaService) {
  }

  ngOnInit() {
  }

  downloadCSV() {
    this.propostaService.readCSV().subscribe(response => {
      // TODO corrigir erro de manipulação da requisição para download
      this.downloadFile(response, 'text/csv');
    }, error => {
      this.downloadFile(error.error.text, 'text/csv');
    });
  }

  private downloadFile(data: any, type: string) {
    console.log(data);
    const blob = new Blob([data], {type});
    const url = window.URL.createObjectURL(blob);
    const pwa = window.open(url);
    if (!pwa || pwa.closed || typeof pwa.closed === 'undefined') {
      alert('Please disable your Pop-up blocker and try again.');
    }
  }

}
