import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {RouterModule} from '@angular/router';
import {NavbarModule} from '../navbar/navbar.module';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    RouterModule,
    NavbarModule
  ],
  exports: [HomeComponent]
})
export class HomeModule {
}
