import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.css']
})
export class ResultadoComponent implements OnInit {
  @Input() taxa: Taxa;
  @Input() ramoAtividade: RamoAtividade;
  @Input() valoresValidos: boolean;


  constructor() {
  }

  ngOnInit() {
  }

  texto() {
    const debitoValido = this.taxa.debito.descontoOferecido > this.ramoAtividade.taxaDebitoMinimo;
    const creditoValido = this.taxa.credito.descontoOferecido > this.ramoAtividade.taxaCreditoMinimo;
    let quantidade = 0;
    let singular = '';
    if (!debitoValido) {
      quantidade++;
      singular = 'A taxa de debito simulada não foi permitidas pois não atingem o mínimo requisitado.';
    }
    if (!creditoValido) {
      quantidade++;
      singular = 'A taxa de crédito simulada não foi permitidas pois não atingem o mínimo requisitado.';
    }
    console.log(quantidade);
    console.log(debitoValido);
    console.log(creditoValido);
    return quantidade === 2 ? 'As taxas simuladas não foram permitidas pois não atingem o mínimo requisitado.' : singular;
  }

}
