import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ResultadoComponent} from './resultado.component';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  declarations: [ResultadoComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [ResultadoComponent]
})
export class ResultadoModule { }
