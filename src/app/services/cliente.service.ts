import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  url = 'http://localhost:8080/cliente';

  constructor(private http: HttpClient) {
  }

  create(cliente: Cliente) {
    return this.http.post<Cliente>(this.url, cliente);
  }

  read() {
    return this.http.get<Cliente[]>(this.url);
  }

  update(cliente: Cliente) {
    return this.http.put<Cliente>(this.url, cliente);
  }

  delete(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }
}
