import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConcorrenteService {
  url = 'http://localhost:8080/concorrente';

  constructor(private http: HttpClient) {
  }

  create(concorrente: Concorrente) {
    return this.http.post<Concorrente>(this.url, concorrente);
  }

  read() {
    return this.http.get<Concorrente[]>(this.url);
  }

  update(concorrente: Concorrente) {
    return this.http.put<Concorrente>(this.url, concorrente);
  }

  delete(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }
}
