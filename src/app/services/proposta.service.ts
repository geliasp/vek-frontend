import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PropostaService {
  url = 'http://localhost:8080/proposta';

  constructor(private http: HttpClient) {
  }

  create(proposta: Proposta) {
    return this.http.post<Proposta>(this.url, proposta);
  }

  read() {
    return this.http.get<Proposta[]>(this.url);
  }

  update(proposta: Proposta) {
    return this.http.put<Proposta>(this.url, proposta);
  }

  delete(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }

  readCSV() {
    return this.http.get<Proposta[]>(`${this.url}/csv`);
  }


}
