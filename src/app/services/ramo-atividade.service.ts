import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RamoAtividadeService {
  url = 'http://localhost:8080/ramoAtividade';

  constructor(private http: HttpClient) {
  }

  create(ramoAtividade: RamoAtividade) {
    return this.http.post<RamoAtividade>(this.url, ramoAtividade);
  }

  read() {
    return this.http.get<RamoAtividade[]>(this.url);
  }

  update(ramoAtividade: RamoAtividade) {
    return this.http.put<RamoAtividade>(this.url, ramoAtividade);
  }

  delete(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }
}
