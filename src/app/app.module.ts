import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NavbarModule} from './navbar/navbar.module';
import {HomeModule} from './home/home.module';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule} from '@angular/forms';
import {SimulacaoModule} from './simulacao/simulacao.module';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NavbarModule,
    HomeModule,
    AppRoutingModule,
    FormsModule,
    SimulacaoModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
