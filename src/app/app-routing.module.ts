import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {SimulacaoComponent} from './simulacao/simulacao.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'simulacao',
    component: SimulacaoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
