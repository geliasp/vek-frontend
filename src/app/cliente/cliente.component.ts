import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})

export class ClienteComponent implements OnInit, OnDestroy {
  @Input() cliente: Cliente;
  @Input() ramoAtividades: RamoAtividade[];

  @Output() valido: EventEmitter<boolean> = new EventEmitter();

  formCliente: FormGroup;


  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.formCliente = this.formBuilder.group({
      telefone: new FormControl(this.cliente.telefone, [Validators.required, Validators.pattern('[0-9]{11}')]),
      email: new FormControl(this.cliente.email, [Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$')]),
      cpf: new FormControl
      (
        this.cliente.cpf,
        [
          Validators.required,
          Validators.pattern(
            '([0-9]{2}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[\\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[-]?[0-9]{2})'
          )
        ]
      ),
      ramoAtividade: new FormControl(this.cliente.ramoAtividade.id, [Validators.required])
    });
    this.atualizar(false);
  }

  ngOnDestroy(): void {
    this.atualizar(true);
  }

  modelChange() {
    this.atualizar(false);
  }

  atualizar(proximo: boolean) {
    this.cliente.cpf = this.cpf.value;
    this.cliente.telefone = this.telefone.value;
    this.cliente.email = this.email.value;
    if (proximo) {
      this.cliente.ramoAtividade.id = this.ramoAtividade.value;
    } else {
      this.cliente.ramoAtividade.id = this.ramoAtividade.value ? this.ramoAtividade.value : undefined;

      this.valido.emit(this.formCliente.status === 'VALID');
    }
  }

  get cpf() {
    return this.formCliente.get('cpf');
  }

  get telefone() {
    return this.formCliente.get('telefone');
  }

  get email() {
    return this.formCliente.get('email');
  }

  get ramoAtividade() {
    return this.formCliente.get('ramoAtividade');
  }

}
