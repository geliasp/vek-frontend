interface Cliente {
  id: any;
  cpf: string;
  email: string;
  telefone: string;
  ramoAtividade: RamoAtividade;
}
