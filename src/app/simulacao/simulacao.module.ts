import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SimulacaoComponent} from './simulacao.component';
import {ClienteModule} from '../cliente/cliente.module';
import {TaxaModule} from '../taxa/taxa.module';
import {SharedModule} from '../shared/shared.module';
import {NavbarModule} from '../navbar/navbar.module';
import {ResultadoModule} from '../resultado/resultado.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [SimulacaoComponent],
    imports: [
        CommonModule,
        ClienteModule,
        TaxaModule,
        SharedModule,
        NavbarModule,
        ResultadoModule,
        FormsModule
    ],
  exports: [SimulacaoComponent]
})
export class SimulacaoModule { }
