import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ConcorrenteService} from '../services/concorrente.service';
import {ClienteService} from '../services/cliente.service';
import {RamoAtividadeService} from '../services/ramo-atividade.service';
import {PropostaService} from '../services/proposta.service';

@Component({
  selector: 'app-simulacao',
  templateUrl: './simulacao.component.html',
  styleUrls: ['./simulacao.component.css']
})
export class SimulacaoComponent implements OnInit {
  ramosAtividades: RamoAtividade[];
  concorrentes: Concorrente[];
  cliente: any;
  taxa: any;
  valido: boolean;

  passo: number;

  lista =
    [
      {
        texto: 'Próximo',
        passo: 1,
        icone: true
      },
      {
        texto: 'Simular',
        passo: 2,
        icone: true
      },
      {
        texto: 'Proposta Aceita',
        passo: 3,
        icone: false
      },
      {
        texto: 'Recusar',
        passo: 0,
        icone: false
      }
    ];

  constructor(
    private concorrenteService: ConcorrenteService,
    private clienteService: ClienteService,
    private ramoAtividadeService: RamoAtividadeService,
    private propostaService: PropostaService,
    private _location: Location,
  ) {
  }

  ngOnInit() {
    this.passo = 0;
    this.valido = false;

    this.ramoAtividadeService.read()
      .subscribe(
        ramoAtividades => {
          this.ramosAtividades = ramoAtividades;
        });

    this.concorrenteService.read()
      .subscribe(
        concorrentes => {
          this.concorrentes = concorrentes;
        }
      );

    this.cliente = {
      cpf: '',
      email: '',
      ramoAtividade: {
        id: undefined,
        nome: '',
        taxaCreditoMinimo: 0,
        taxaDebitoMinimo: 0
      },
      telefone: ''
    };

    this.taxa = {
      concorrente: {
        id: undefined,
        nome: ''
      },
      credito: {
        descontoOferecido: undefined,
        taxaConcorrente: undefined
      },
      debito: {
        descontoOferecido: undefined,
        taxaConcorrente: undefined
      }
    };
  }

  passoChangedHandler(passo: number) {
    this.passo = passo;
  }

  validoChangedHandler(valido: boolean) {
    this.valido = valido;
  }

  proximo() {
    if (!!this.lista[this.passo]) {
      this.passo = this.lista[this.passo].passo;
    }
  }

  anterior() {
    this.passo--;
  }

  valoresValidos() {
    if (this.lista[this.passo].passo === 3) {
      const ramoAtividadeSelecionado = this.ramosAtividades.find(x => x.id === this.cliente.ramoAtividade.id);
      const debitoValido = this.taxa.debito.descontoOferecido > ramoAtividadeSelecionado.taxaDebitoMinimo;
      const creditoValido = this.taxa.credito.descontoOferecido > ramoAtividadeSelecionado.taxaCreditoMinimo;
      return debitoValido && creditoValido;
    } else {
      return true;
    }
  }

  ramoAtividade() {
    return this.ramosAtividades.find(x => x.id === this.cliente.ramoAtividade.id);
  }

  propostaAceita() {
    this.salvarProposta(true);
  }

  propostaRecusada() {
    this.salvarProposta(false);
  }

  salvarProposta(aceita: boolean) {
    this.cliente.ramoAtividade = this.ramosAtividades.find(x => x.id === this.cliente.ramoAtividade.id);
    this.taxa.concorrente = this.concorrentes.find(x => x.id === this.taxa.concorrente.id);
    this.clienteService.create(this.cliente).subscribe(cliente => {

      const proposta: Proposta = {
        fkCliente: cliente.id,
        fkRamoAtividade: this.cliente.ramoAtividade.id,
        fkConcorrente: this.taxa.concorrente.id,
        creditoConcorrente: this.taxa.credito.taxaConcorrente,
        debitoConcorrente: this.taxa.debito.taxaConcorrente,
        creditoProposta: this.taxa.credito.descontoOferecido,
        debitoProposta: this.taxa.debito.descontoOferecido,
        aceita
      };

      this.propostaService.create(proposta).subscribe(retorno => {
        console.log(retorno);
        this._location.back();
      }, error => {
        console.log(error);
      });
    });


  }

}
