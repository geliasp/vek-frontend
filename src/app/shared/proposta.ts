interface Proposta {
  fkCliente: number;
  fkRamoAtividade: number;
  fkConcorrente: number;
  creditoConcorrente: number;
  debitoConcorrente: number;
  creditoProposta: number;
  debitoProposta: number;
  aceita: boolean;
}
