interface RamoAtividade {
  id: number;
  nome: string;
  taxaCreditoMinimo: number;
  taxaDebitoMinimo: number;
}
