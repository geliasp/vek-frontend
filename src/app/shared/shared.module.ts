import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppRoutingModule} from '../app-routing.module';
import {TituloComponent} from './titulo/titulo.component';

@NgModule({
  declarations: [
    TituloComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  exports: [
    TituloComponent
  ]
})
export class SharedModule {
}
